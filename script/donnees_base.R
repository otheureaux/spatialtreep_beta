## Wébinaire sur R par LAMARANGE
# https://www.youtube.com/channel/UCF9ymC7Dpjwr0lge2QR_A5Q

# Selection du dossier
setwd("/Users/olivier/Documents/GEO/MASTER/P7/02-STATISTIQUES/TD4/Regression_lineaire_V0")
setwd("~/Documents/GEO/MASTER/P7/02-STATISTIQUES/TD4/Regression_lineaire_V0")
list.files()

# couper l'écran en plusieurs carrés----
par(mfrow=c(2,1))     # Division de la fenêtre graphique en deux colonnes
par(mfcol=c(1,2))     # Division de la fenêtre graphique en deux colonnes

### Classes de donnees -----
str(donnees)
?str()
# caractères (type = character) : character ou strings : chaîne de caractères, comme un identifiant unique : un prénom 
# nombre réel ou décimal / num (type double) : nombre réel avec décimales, nombre que l'on peut additionner
# nombre entier (type = integer) nombre entier, nombre que l'on peut additionner
# dates : as.date
# valeur logique (type = logical) : condition binaire TRUE/FALSE. Oui ou Non : ex : a-t-il voyager à l’étranger dans les 12 derniers mois

## D'autres type d'objet
# vecteur
# liste
# matrice (c'est un vecteur)
# data frame (c'est une liste)

## facteur ou factor un objet particulier : un type de vecteur particulier
# facteur ou factor : catégorie (ou classe) comportant un numéro (integer) et une étiquette (le nom de la catégorie = région, pays, département)
# pour identifier les facteur
levels(donnees) # ici NULL car il faut spécifier autre chose que donnees

# NA : Not Available
# NaN : Not a Number
### convertir des donnees
# as.numeric, as.quelquechose

## Lire un fichier -----

# EXCEL
library ("readxl")
donnees <- read_excel("donnees/trial.xlsx")

# lire un fichier CSV
donnees <- read.csv("vdm_data.csv",
                    header=TRUE,
                    sep=";",
                    dec=",",
                    encoding="UTF8",
                    stringsAsFactors = F)
donnees <- read.table("vdm_data.csv",
                    header=TRUE,
                    sep=";",
                    dec=",",
                    encoding="UTF8",
                    stringsAsFactors = F)

# Calcul sur données quantitatives -----

# effectifs cumulés 
granulo <- read_excel("granulometrie.xlsx")
mean(granulo$Granulométrie)
effectif <- table(granulo$Granulométrie)
frequence <- effectif*100/100
frequence
effcum <- cumsum(effectif)
effcum*100/100
plot(effcum, 
     xlab = "Granulométrie", 
     ylab = "Effectifs cumulés")

# Calcul sur les données qualitatives -----

View(X)
print(X)
head(donnees, 10)
tail(donnees)
summary(X)
hist(X)
rug(X)
length(X)
var(X)
sd(X)
colnames(donnees)
str(donnees)
donnees <- as.numeric(donnees$code)
print(donnees$code)

## Variable qualitative -----
## tableau de dénombrement (effectif)
table(donnees$COM)
xtabs(~ stage, data = trial)

# Fréquence 
library(questionR)
freq(trial@age, cum = T, total = T)

## Selectionner des éléments dans un tableau
# source : https://bookdown.org/fousseynoubah/dswr_book/objets-dans-r.html

# LE premier et le troisième
age_arrivee_pouvoir[c(1, 3)]

# du 1er au 3ème président
age_arrivee_pouvoir[c(1:3)]

# exclure certains éléments
age_arrivee_pouvoir[-c(4, 5)]

## Graphique
plot(X,Y, main="cool") 
boxplot(X)
pie(X)
hist(X)
rug(X)

x<-1:30
x <- c(1,2,3,4,5)

# STATS Création de tableau descriptif : gtsummary ----

library(gtsummary)
tbl_summary(trial, include = c("age", "stage"))
# cliquer sur "show in a new window" 3 icônes à droite d'Export. Et copier dans Word
theme_gtsummary_language("fr", decimal.mark = "," , big.mark = " ")

# données EAU et AIR M2 au LGP

tbl_summary(donnees)
tbl_summary(donnees, include = c("phosphates", "turbidite_moyenne"), by = nom, statistic = list(all_continuous() ~ "{mean}"))

# Graphique bivariés -----
library(ggplot2)
library(esquisse)

#tout d'abord on récupère ces noms :
nom<-donnees$COM_NOM

#ensuite on les ajoute au graphe
text(X,Y,
        labels=nom,    # nom à ajouter sur le graphique
        adj=c(0.5,-1), # position par rapport au point
        cex=0.5)       # coefficient multiplicateur de taille

# Jolie boxplot
data("ChickWeight")
chick <- ChickWeight
boxplot (chick$weight~chick$Diet,
         main = "la plus belle boxplot du monde",
         cex.main = 2, # taille de police du titre
         col.main = "pink",
         col = heat.colors(4), # couleur des points
         lwd = 1, # change l'épaisseur des lignes
         border = "grey",
         col.axis = "blue", # couleur des axes
         cex.axis = 1.5,# taille police des nb des axes
         cex.lab = 2, # taille de police des labels
         xlab = "Régimes",
         ylab = "Poids des piou-piou")

boxplot (chick$weight~chick$Diet,
         main = "la plus belle boxplot du monde",
         cex.main = 2, # taille de police du titre
         col.main = "pink",
         col = "blue", # couleur des points
         lwd = 1, # change l'épaisseur des lignes
         border = "grey",
         col.axis = "blue", # couleur des axes
         cex.axis = 1.5,# taille police des nb des axes
         cex.lab = 2, # taille de police des labels
         xlab = "Régimes",
         ylab = "Poids des piou-piou")

# couper l'écran en plusieurs carrés----
par(mfrow=c(2,1))     # Division de la fenêtre graphique en deux colonnes
par(mfcol=c(1,2))     # Division de la fenêtre graphique en deux colonnes

# ajouter une carte sur une autre carte
par(new=T) # pour annuler  par(new=F)
plot (add = T)

#  STATS Corrélation -----
 
cor(X,Y)
cor.test(temps, moyenne_annuelle)
## : test de Bravais-Pearson
# Si p-value est grande = pas de corrélation
#Ici p-value est très petite donc corrélation

## CARTO Définition du CRS -----

# consulter le CRS
st_crs(zt_ravines)
# reprojeter
ravines_reproj <- st_transform(ravines, 27572)


# STATS : tbl_summary

taille <- c(1.88, 1.67, 1.78, 1.7, 1.68, 1.74)
age <- c(34, 35, 27, 28, 45, 47)
taille[2]
p <- bind_cols(taille, age)
p <- rename(p, taille = 1, age = 2 )
p

tbl_summary(p)

# MAPVIEW pour MGP ----

library(tmap)
library(dplyr)
library(sf)
library(mapview)
library(cartography)

setwd("~/Documents/GEO/MASTER/M2/ATELIER_GR_PARIS/QGIS")
list.files()

MGP <- st_read("SHP/contours_MGP/contours_MGP.shp", package="sf")
MGP2 <- st_read("Limite MGP/MGPV2-3949.shp", package = "sf")
territoires_MGP <-st_read("Territoires_MGP.shp/Territoires_MGP.shp")
Stations_hydro <- st_read("SHP/stations_hydro/station_hydro.shp")
Stations_hydro_actives <- Stations_hydro %>% filter(Etat.stati =="active")
reseau_hydro <- st_read("SHP/Cours_eau_MGP/cours_eau_mgp.shp")

# toutes les stations
mapview(st_geometry(MGP),
        col.regions = "red") + 
        mapview(Stations_hydro, 
                col.regions = "blue") +
        mapview(st_geometry(reseau_hydro), 
                col.regions = "blue")

# uniquement les stations actives
mapview(st_geometry(MGP),
        col.regions = "red" ) + 
        mapview(Stations_hydro_actives, col.regions = "blue") +
        mapview (reseau_hydro, col.regions = "blue")

?mapview

# package cartography

MGP
str(MGP)

plot(st_geometry(territoires_MGP))
plot(st_geometry(MGP), 
     north(pos = c(2.6, 48.7)), 
     barscale(size = 10))

MGP$
        
        st_crs(MGP)

# tmap - cartographie ----

tm_shape(MGP) + 
        tm_fill() 

tm_shape(MGP) + 
        tm_borders() 

tm_shape(points) +
  tm_dots()

qtm(nz) +
        qtm(nz_height)


### GGPLOT ----
# Source : http://www.sthda.com/french/wiki/ggplot2-combiner-plusieurs-graphiques-sur-la-m-me-page-logiciel-r-et-visualisation-de-donn-es

install.packages("gridExtra")
library("gridExtra")
install.packages("cowplot")
library("cowplot")

setwd("/Users/olivier/Documents/GEO/MASTER/ton_fichier.xls", header =T, sep=";", dec = ",", na.strings = "#NA")
setwd("/Users/olivier/Documents/GEO/MASTER/ton_fichier.xls")

library(readxl)
install.packages("ggplot2")
library(ggplot2)
table <- read.csv("test_mirabelle.csv", sep = ";")
list.files()
plot(table)

f <- ggplot(table, aes(ï..date, temp1))
f <- f + geom_col()
       
g <- ggplot(table, aes(ï..date, temp2))
g <- g + geom_col()

h <- ggplot(table, aes(ï..date, temp3))
h <- h + geom_col()

i <- ggplot(table, aes(ï..date, temp2))
i <- i + geom_col()

plot_grid(f, g, h, i, labels=c("A", "B", "C", "D"))

### COMBLER VALEURS ----

table <- read.csv("test_geoteka.csv", sep = ";")

new_table <- table %>%
  mutate(new_espece = case_when(is.na(espece) ~ code, 
                                espece = NA ~ espece, 
                                espece =! NA))

# Calcul NDVI







